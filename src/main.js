import Vue from 'vue'
import App from './App.vue'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import Vuelidate from 'vuelidate'
import store from './store'

var firebase = require('firebase/app');

Vue.use(BootstrapVue);
Vue.use(Vuelidate);

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

require('firebase/app');
require('firebase/auth');
require('firebase/firestore');
require('firebase/storage');

const firebaseConfig = {
    apiKey: "AIzaSyAj6MgNKPHlD3TmozAYgzPGMotZvPPkMmY",
    authDomain: "crud-vue-76c9e.firebaseapp.com",
    databaseURL: "https://crud-vue-76c9e.firebaseio.com",
    projectId: "crud-vue-76c9e",
    storageBucket: "crud-vue-76c9e.appspot.com",
    messagingSenderId: "185298663753",
    appId: "1:185298663753:web:cccf06488c650a64970298"
};

// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);

export default firebaseApp.firestore()

Vue.config.productionTip = false;

firebase.auth().onAuthStateChanged((user) => {
    if (user) {
        store.dispatch('verifyActiveSession', {email: user.email, uid: user.uid})
    } else {
        store.dispatch('verifyActiveSession', null)
    }

    new Vue({
        router,
        store,
        render: h => h(App)
    }).$mount('#app')
});
