import Vue from 'vue'
import VueRouter from 'vue-router'

const firebase = require('firebase/app');

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'start',
        meta: {requiresAuth: true}, //Protected route
        component: () => import(/* webpackChunkName: "about" */ '../views/Start.vue')
    },
    {
        path: '/register',
        name: 'register',
        component: () => import(/* webpackChunkName: "about" */ '../views/Register.vue')
    },
    {
        path: '/login',
        name: 'login',
        component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue')
    },
    {
        path: '/edit/:id',
        name: 'edit',
        meta: {requiresAuth: true},
        component: () => import(/* webpackChunkName: "about" */ '../views/Edit.vue')
    },
    {
        path: '/create',
        name: 'create',
        meta: {requiresAuth: true},
        component: () => import(/* webpackChunkName: "about" */ '../views/Create.vue')
    },
    {
        path: '/delete',
        name: 'delete',
        meta: {requiresAuth: true},
        component: () => import(/* webpackChunkName: "about" */ '../views/Create.vue')
    },
    {
        path: '/admin',
        name: 'admin',
        meta: {requiresAuth: true},
        component: () => import(/* webpackChunkName: "about" */ '../views/Admin.vue')
    },
    {
        path: '/chat',
        name: 'chat',
        meta: {requiresAuth: true},
        component: () => import(/* webpackChunkName: "about" */ '../views/Chat.vue')
    }
]

const router = new VueRouter({
    routes
});

router.beforeEach((to, from, next) => {
    const protectedRoute = to.matched.some(record => record.meta.requiresAuth); //Verify if route has meta
    const user = firebase.auth().currentUser //Verify if exist user

    if (protectedRoute && user == null) {
        next({name: 'login'})
    }else {
        next()
    }

});

export default router
