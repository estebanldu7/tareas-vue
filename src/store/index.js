import Vue from 'vue'
import Vuex from 'vuex'
import db from '../main'
import router from "../router";
import {storage} from "firebase";

const firebase = require('firebase/app');
require('firebase/auth');

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        tasks: [],
        taskToEdit: {name: '', id: ''},
        user: '',
        error: '',
        text: '',
        file: null,
        getInfo: false,
        tempUrl: '',
    },

    mutations: {
        setTasks(state, tasks) {
            state.tasks = tasks
        },

        setTask(state, taskToEdit) {
            state.taskToEdit = taskToEdit
        },

        taskToDelete(state, id) {
            state.tasks = state.tasks.filter(doc => {
                return doc.id != id
            })
        },

        setUser(state, payload) {
            state.user = payload
        },

        setError(state, payload) {
            state.error = payload
        },

        verifyInfoCharged(state, payload) {
            state.getInfo = payload
        }
    },

    actions: {
        getTasks({commit}, payload) {

            commit('verifyInfoCharged', true); //verify if tasks is downloaded from firebase

            const user = firebase.auth().currentUser;
            const tasks = [];

            db.collection(user.email).get()
                .then(snapshop => {
                    snapshop.forEach(doc => {
                        let task = doc.data();
                        task.id = doc.id;
                        tasks.push(task)
                    });

                    setTimeout(() => {
                        commit('verifyInfoCharged', false);
                    }, 1000);
                });

            commit('setTasks', tasks)
        },

        getTask({commit}, id) {
            const user = firebase.auth().currentUser;

            db.collection(user.email).doc(id).get()
                .then(doc => {
                    let taskToEdit = doc.data();
                    taskToEdit.id = doc.id;
                    commit('setTask', taskToEdit)
                });
        },

        updateTask({commit}, taskToEdit) {
            const user = firebase.auth().currentUser;

            db.collection(user.email).doc(taskToEdit.id).update({
                name: taskToEdit.name
            }).then(() => {
                router.push({name: 'start'})
            })
        },

        createTask({commit}, newTask) {
            const user = firebase.auth().currentUser;

            db.collection(user.email).add({
                name: newTask
            }).then(doc => {
                router.push({name: 'start'})
            })
        },

        deleteTask({commit}, taskToDelete) {
            const user = firebase.auth().currentUser;

            db.collection(user.email).doc(taskToDelete).delete()
                .then(() => {
                    commit('taskToDelete', taskToDelete)
                })
        },

        createUser({commit}, payload) {
            firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
                .then(res => {
                    commit('setUser', {email: res.user.email, uid: res.user.uid});

                    db.collection(res.user.email).add({
                        name: 'Tarea de ejemplo',
                        picture: ''
                    }).then(doc => {
                        router.push({name: 'start'})
                    })

                }).catch(err => {
                commit('setError', err.message)

            })
        },

        loginUser({commit}, payload) {
            firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
                .then(res => {
                    router.push({name: 'start'})
                }).catch(err => {
                commit('setError', err.message)
            })
        },

        verifyActiveSession({commit}, payload) {
            if (payload != null) {
                commit('setUser', {email: payload.email, uid: payload.uid})
            } else {
                commit('setUser', null)
            }
        },

        logoutUser({commit}, payload) {
            firebase.auth().signOut();
            commit('setUser', null)
            router.push({name: 'login'})

        },

        async loginGoogle({commit}) {

            const provider = new firebase.auth.GoogleAuthProvider();
            firebase.auth().languageCode = 'es';

            try {
                const result = await firebase.auth().signInWithPopup(provider);
                const user = result.user;

                //build user
                const userObject = {
                    name: user.displayName,
                    email: user.email,
                    uid: user.uid,
                    picture: user.photoURL
                };

                //save in firestore
                await db.collection(userObject.email).add({
                    name: 'Tarea de ejemplo',
                    picture: userObject.picture
                }).then(doc => {
                    router.push({name: 'start'})
                })
            } catch (e) {
                console.log(e)
            }
        },

        async loginFacebook({commit}) {
            const provider = new firebase.auth.FacebookAuthProvider();
            firebase.auth().languageCode = 'es';

            try {
                const result = await firebase.auth().signInWithPopup(provider);
                const user = result.user;

                //build user
                const userObject = {
                    name: user.displayName,
                    email: user.email,
                    uid: user.uid,
                    picture: user.photoURL
                };

                //save in firestore
                await db.collection(userObject.email).add({
                    name: 'Tarea de ejemplo',
                    picture: userObject.picture
                }).then(doc => {
                    router.push({name: 'start'})
                })
            } catch (e) {
                console.log(e)
            }
        },

        searchTasks({commit, state}, payload) {
            state.text = payload.toLowerCase()
        },

        searchImage({commit}, payload) {

            const filetype = payload.target.files[0].type

            if(filetype === 'image/jpeg' || filetype === 'image/png'){
                this.state.file = payload.target.files[0];
            }else{
                this.state.error = 'Imagen con formato no valido'
            }

            const reader = new FileReader();
            reader.readAsDataURL(this.state.file);
            reader.onload = (e) => {
                this.state.tempUrl = e.target.result
            }
        },

        async uploadImage({commit}, payload) {
            try {
                const refImage = storage().ref().child(this.state.user.email).child('profile image');
                const res = await refImage.put(this.state.file);
                const urlDownload = await refImage.getDownloadURL();
                this.state.tasks.picture = urlDownload;

                await db.collection(this.state.user.email).doc(payload.id).update({
                    picture: urlDownload
                })
            } catch (e) {
                console.log(e)
            }
        },

        sendChatMessage({commit}, payload){
            console.log(payload)
        }
    },

    getters: {
        filterArray(state) {
            let filetArr = [];

            for (let task of state.tasks) {
                let name = task.name.toLowerCase();

                if (name.indexOf(state.text) >= 0) {
                    filetArr.push(task)
                }
            }

            return filetArr;
        }
    }
})
